
# Estudos

A cada mês um novo conjunto de estudos é proposto para fornecer novas experiências de programação e conhecimento para todos integrantes.



## Estudos

 - [Flutter](https://stealth-pumpkin-fef.notion.site/Flutter-d838bebab8f44366af867e87f22b3b9d)
 - [NestJS](https://stealth-pumpkin-fef.notion.site/NestJS-2ee194d091e340f3a7eefbb5a1e17b6a)
 - [Micro-Serviços](https://stealth-pumpkin-fef.notion.site/Micro-Servi-o-9175c607ab0e45a8910cf03d6d728480)
 - [Micro-Frontends](https://stealth-pumpkin-fef.notion.site/Micro-Frontends-93ca61da39d549cbab765f12234b951f)
 - [Interfaces Multitematicas](https://stealth-pumpkin-fef.notion.site/Interfaces-multitem-ticas-56044a28ecfd4c8ebe827d72d476ef91)
 - [Animações](https://stealth-pumpkin-fef.notion.site/Interfaces-multitem-ticas-56044a28ecfd4c8ebe827d72d476ef91)


## Cursos de Abril

- NestJS 
- Micro-Frontends
- Animações

## FAQ

#### Estudos são obrigatorios?

Os estudos são fornecidos como atividades complementares ao trabalho, não sendo obrigatorio e nem requisitado como demanda das sprints.

#### Posso criar respositorios?

A ideia é criar os respositorios dentro dos sub-grupos para que os outros desenvolvedores do time possam visualizar e também contribuir

#### Como posso ter acesso as plataformas?

Todos os acessos foram colocados no grupo do teams, caso não tenha conseguido fazer o login basta me enviar uma mensagem que passo

Aconselho usar uma diferente de outro colega, para que possa ver diferentes exemplos de implementação.

#### Não estou conseguindo criar repositorios?

Me manda seu nome do gitlab no teams para que possa o adicionar no grupo.

#### Vai existir alguma data para rever os estudos?

Sim, a ideia é fazer esse processo na ultima sexta feira de cada mês.

#### Posso sugerir um tema?

Sim, pode mandar os temas no grupo do teams, nas votações ou me mandar diretamente que vou incluindo na lista.

## Feedback

Se você tiver algum feedback, por favor nos deixe saber por meio de caio.henrique@globalfinanceiro.com

